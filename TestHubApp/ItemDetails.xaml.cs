﻿using TestHubApp.Common;
using TestHubApp.DataModel;
using TestHubApp.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI;
using System.Windows;
using System.Text;
using Windows.UI.Xaml.Markup;



// The Hub Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=321224

namespace TestHubApp
{
    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class ItemDetails : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private static Section4Model section4Model=new Section4Model();

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public ItemDetails()
        {
            this.InitializeComponent();
            

           

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
        }

      


        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            DefaultViewModel["ItemDetail"] =await Section3DataSource.GetItemAsync(((string)e.NavigationParameter));

            Section2Model section2Model = await Section2DataSource.GetSection2Async(((string)e.NavigationParameter));
            if (section2Model == null) section2Model = await Section2DataSource.GetSection2Async("ID1");
            Section2.DataContext = section2Model;

            section4Model = await Section4DataSource.GetSection4Async("ID1");
            this.Section4.DataContext = section4Model;
            section4Model.SelectedImage = section4Model.Images[0].ImagePath;
            section4Model.SelectedDescription = section4Model.Images[0].Description;
            section4Model.SelectedTitle = section4Model.Images[0].Title;
            section4Model.SelectedImageId = section4Model.Images[0].UniqueId;




            this.CreateSection6Async();
            // TODO: Assign a collection of bindable groups to this.DefaultViewModel["Groups"]
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion



        #region Section4EventHandlers
        private void Section4Control_Loaded(object sender, RoutedEventArgs e)
        {
            Section4Control iStack = (Section4Control)sender;
            iStack.ItemClicked += iStack_ItemClicked;
            iStack.ImageTapped += iStack_ImageTapped;
            iStack.ImagePointerEntered += iStack_ImagePointerEntered;
            iStack.ImagePointerExited += iStack_ImagePointerExited;
        }

        void iStack_ImagePointerExited(object sender, PointerRoutedEventArgs e)
        {
            section4Model.BorderBackgroundBrush = new SolidColorBrush(Colors.Transparent);
        }

        void iStack_ImagePointerEntered(object sender, PointerRoutedEventArgs e)
        {
            section4Model.BorderBackgroundBrush = new SolidColorBrush(Colors.Gray);
        }

        void iStack_ImageTapped(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ImageMedia), section4Model);
        }

        async void iStack_ItemClicked(object sender, ItemClickEventArgs e)
        {
            string imageId = ((Section4Image)e.ClickedItem).UniqueId;

            Section4Image image = await Section4DataSource.GetSection4ImageAsync(imageId);
            section4Model.SelectedImage = image.ImagePath;
            section4Model.SelectedDescription = image.Description;
            section4Model.SelectedTitle = image.Title;
            section4Model.SelectedImageId = image.UniqueId;
        }

        #endregion Section4EventHandlers


        #region Section6Creation
        private async void CreateSection6Async()
        {



            Section6Model section6Model = await Section6DataSource.GetSection6Async("ID1");

            GenerateParagraph generateParagraph = new GenerateParagraph();

            Section1.ContentTemplate = generateParagraph.GetDataTemplate(section6Model);
            Section1.DataContext = section6Model;
            section6Model.GridWidth = generateParagraph.TotalChar * 400.0;

        }
        #endregion Section6Creation

    }
}
