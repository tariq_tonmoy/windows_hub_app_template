﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace TestHubApp
{
    public sealed partial class Section5Control : UserControl
    {
        public delegate void ActionControl(object sender, ItemClickEventArgs e);
        public event ActionControl VideoClicked;
        public Section5Control()
        {
            this.InitializeComponent();
        }

        private void VideoView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (VideoClicked != null) VideoClicked(sender, e);

        }
    }
}
