﻿using TestHubApp.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TestHubApp.DataModel;
using System.Threading.Tasks;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace TestHubApp
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    /// 
    
    public sealed partial class ImageMedia : Page
    {

        private static bool flag = false,descFlag=false;
        private static Section4Model _section4Model=new Section4Model();

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public ImageMedia()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            
            if (e.NavigationParameter.GetType().Equals((new Section4Model()).GetType()))
            {
                flag = false;

                Section4Model section4Model = (Section4Model)e.NavigationParameter;
                _section4Model = section4Model;
                //Task<Section4Image> section4Image = Section4DataSource.GetSection4ImageAsync(section4Model.SelectedImageId);
                //DefaultViewModel["ImageSource"] = await section4Image;
                ////System.Diagnostics.Debug.WriteLine(_section4Model.SelectedImage);
                //Grid2.DataContext=_section4Model;

                DefaultViewModel["ImageSource"] = _section4Model;
                
                Canvas1.Visibility = Canvas2.Visibility = Grid1.Visibility = Grid2.Visibility = descriptionScroll.Visibility = infoButton.Visibility = Visibility.Collapsed;

            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            flag=!flag;
            if (flag == true)
            {
                Canvas1.Visibility = Canvas2.Visibility = Grid1.Visibility = Grid2.Visibility = infoButton.Visibility = Visibility.Visible;

                backButton.IsEnabled = true;
                infoButton.IsEnabled = true;
                gridView.IsEnabled = true;
                StoryboardFadeIn.Begin();
            }
            else
            {
                //Canvas1.Visibility = Canvas2.Visibility = Grid1.Visibility = Grid2.Visibility = descriptionScroll.Visibility = infoButton.Visibility = Visibility.Collapsed;

                backButton.IsEnabled = false;
                infoButton.IsEnabled = false;
                gridView.IsEnabled = false;
                StoryboardFadeOut.Begin();

            }
            if (!flag) descFlag = false;
        }

        private async void ImageView_ItemClick(object sender, ItemClickEventArgs e)
        {
            string imageId = ((Section4Image)e.ClickedItem).UniqueId;

            Section4Image image = await Section4DataSource.GetSection4ImageAsync(imageId);
            _section4Model.SelectedImage = image.ImagePath;
            _section4Model.SelectedDescription = image.Description;
            _section4Model.SelectedTitle = image.Title;
            _section4Model.SelectedImageId = image.UniqueId;
            imageStoryboard.Begin();
            flag = !flag;
        }

        private void infoButton_Click(object sender, RoutedEventArgs e)
        {
           
            flag = !flag;
            descFlag = !descFlag;
            if (descFlag == false)
            {
                descriptionScroll.Visibility = Visibility.Visible;

                scrollShowStoryboard.Stop();
                scrollHideStoryboard.Begin();
            }
            else
            {
                descriptionScroll.Visibility = Visibility.Visible;
                //scrollShowStoryboard.Begin();
               
                //descriptionScroll.Margin = new Thickness(306, 6, 0, 24);
                scrollHideStoryboard.Stop();
                StoryboardFadeOut.Stop();
                scrollShowStoryboard.Begin();
            }
        }

        private void scrollHideStoryboard_Completed(object sender, object e)
        {
           
        }

        private void Image_Loaded(object sender, RoutedEventArgs e)
        {
            imageStoryboard.Begin();
        }
    }
}
