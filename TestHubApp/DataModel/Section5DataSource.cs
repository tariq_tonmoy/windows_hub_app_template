﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
//using TestHubApp.Data;


namespace TestHubApp.DataModel
{
    public class Section5DataSource
    {
        private static Section5DataSource _section5DataSource = new Section5DataSource();
        private ObservableCollection<Section5Model> _section5Models = new ObservableCollection<Section5Model>();
        
        public ObservableCollection<Section5Model> Section5Models
        {
            get { return _section5Models; }
            set { _section5Models = value; }
        }



        public static async Task<Section5Video> GetSection5VideoAsync(string uniqueId)
        {
            await _section5DataSource.GetAllSection5Model();
            var match = _section5DataSource.Section5Models.SelectMany(x => x.VideoList).Where(x => x.UniqueId == uniqueId);
            if (match.Count() == 1) return match.First();
            return null;
        }
        public static async Task<Section5Model> GetSection5Async(string uniqueId)
        {
            await _section5DataSource.GetAllSection5Model();
            var match = _section5DataSource._section5Models.Where(x => x.UniqueId.Equals(uniqueId));
            if (match.Count() == 1)
            {
                return match.First();
            }
            return null;
        }

        private async Task GetAllSection5Model()
        {
            if (this._section5Models.Count != 0)
                return;

            Uri dataUri = new Uri("ms-appx:///Data/Section5Data.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["Section5"].GetArray();

            foreach (JsonValue section5Value in jsonArray)
            {
                JsonObject section5Object = section5Value.GetObject();
                Section5Model tempModel = new Section5Model()
                {
                    UniqueId = section5Object["UniqueId"].GetString(),
                    Header = section5Object["Header"].GetString(),
                    VideoList = this.GetSection5Video(section5Object)

                };



                this._section5Models.Add(tempModel);
            }
        }

        private ObservableCollection<Section5Video> GetSection5Video(JsonObject section5Object)
        {
            if (section5Object == null)
                return null;
            ObservableCollection<Section5Video> _section5VideoList = new ObservableCollection<Section5Video>();
            foreach (JsonValue item in section5Object["Video"].GetArray())
            {
                JsonObject videoObject = item.GetObject();
                Section5Video section5Video = new Section5Video()
                {
                    UniqueId = videoObject["UniqueId"].GetString(),
                    YouTubeId = videoObject["YouTubeId"].GetString(),
                    Title = videoObject["Title"].GetString()

                };

                _section5VideoList.Add(section5Video);

            }

            return _section5VideoList;

        }


    }
    public class Section5Model
    {
        string _uniqueId, _header;
        ObservableCollection<Section5Video> _videoList = new ObservableCollection<Section5Video>();

        public string UniqueId
        {
            get { return _uniqueId; }
            set { _uniqueId = value; }
        }

        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }
        

        public ObservableCollection<Section5Video> VideoList
        {
            get { return _videoList; }
            set { _videoList = value; }
        }

     

    }

    public class Section5Video
    {
        string _title, _uniqueId, _videoPath, _thumbNailPath, _youTubeId;

        public string YouTubeId
        {
            get { return _youTubeId; }
            set
            {
                VideoPath = value;
                ThumbNailPath = value;
                _youTubeId = value; 
            }
        }

        public string ThumbNailPath
        {
            get { return _thumbNailPath; }
            set { _thumbNailPath = "http://img.youtube.com/vi/"+value+"/0.jpg"; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string UniqueId
        {
            get { return _uniqueId; }
            set { _uniqueId = value; }
        }

        public string VideoPath
        {
            get { return _videoPath; }
            set { _videoPath = "http://www.youtube.com/embed/"+value; }
        }
    }
}
