﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI;

// The data model defined by this file serves as a representative example of a strongly-typed
// model.  The property names chosen coincide with data bindings in the standard item templates.
//
// Applications may use this model as a starting point and build on it, or discard it entirely and
// replace it with something appropriate to their needs. If using this model, you might improve app 
// responsiveness by initiating the data loading task in the code behind for App.xaml when the app 
// is first launched.

namespace TestHubApp.DataModel
{
    /// <summary>
    /// Generic item data model.
    /// </summary>
    public class Section3Item
    {
        public Section3Item(String uniqueId, String title, String subtitle, String imagePath, String description, String content)
        {
            this.UniqueId = uniqueId;
            this.Title = title;
            this.Subtitle = subtitle;
            this.Description = description;
            this.ImagePath = imagePath;
            this.Content = content;
        }

        public string UniqueId { get; private set; }
        public string Title { get; private set; }
        public string Subtitle { get; private set; }
        public string Description { get; private set; }
        public string ImagePath { get; private set; }
        public string Content { get; private set; }

        public override string ToString()
        {
            return this.Title;
        }
    }

    //public class CommonNotifyPropertyChanged : INotifyPropertyChanged
    //{

    //    public event PropertyChangedEventHandler PropertyChanged;

    //    protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName]string propertyName=null)
    //    {
    //        if (object.Equals(storage, value)) return false;
    //        storage = value;
    //        this.OnPropertyChanged(propertyName);

    //        return true;

    //    }


    //    private void OnPropertyChanged(string propertyName)
    //    {
    //        if (this.PropertyChanged != null)
    //        {
    //            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    //        }

    //    }        

    //}


    //public class MyViewModel:CommonNotifyPropertyChanged
    //{
    //    SolidColorBrush textForegroundBrush, buttonBackgroundBrush;
    //    double gridWidth;

    //    public double GridWidth
    //    {
    //        get { return gridWidth; }
    //        set 
    //        {
    //            SetProperty(ref gridWidth, value); 
            
    //        }
    //    }
    //    public SolidColorBrush ButtonBackgroundBrush
    //    {
    //        get
    //        {
                
    //            return buttonBackgroundBrush;
    //        }

    //        set 
    //        {
    //            SetProperty(ref buttonBackgroundBrush, value);
    //        }
    //    }

    //    public SolidColorBrush TextForegroundBrush
    //    {
    //        get
    //        {
    //            return textForegroundBrush;
    //        }

    //        set
    //        {
    //            SetProperty(ref textForegroundBrush, value);
    //        }
    //    }
    //}
    /// <summary>
    /// Generic group data model.
    /// </summary>
    public class Section3Model
    {
        public Section3Model(String uniqueId, String title, String subtitle, String imagePath, String description)
        {
            this.UniqueId = uniqueId;
            this.Title = title;
            this.Subtitle = subtitle;
            this.Description = description;
            this.ImagePath = imagePath;
            this.Items = new ObservableCollection<Section3Item>();
        }

        public string UniqueId { get; private set; }
        public string Title { get; private set; }
        public string Subtitle { get; private set; }
        public string Description { get; private set; }
        public string ImagePath { get; private set; }
        public ObservableCollection<Section3Item> Items { get; private set; }

        public override string ToString()
        {
            return this.Title;
        }
    }

    /// <summary>
    /// Creates a collection of groups and items with content read from a static json file.
    /// 
    /// SampleDataSource initializes with data read from a static json file included in the 
    /// project.  This provides sample data at both design-time and run-time.
    /// </summary>
    public  class Section3DataSource
    {
        private static Section3DataSource _sampleDataSource = new Section3DataSource();

        private ObservableCollection<Section3Model> _groups = new ObservableCollection<Section3Model>();
        public ObservableCollection<Section3Model> Groups
        {
            get { return this._groups; }
        }

        public static async Task<IEnumerable<Section3Model>> GetGroupsAsync()
        {
            await _sampleDataSource.GetAllSection3Async();

            return _sampleDataSource.Groups;
        }

        public static async Task<Section3Model> GetSection3Async(string uniqueId)
        {
            await _sampleDataSource.GetAllSection3Async();
            // Simple linear search is acceptable for small data sets
            var matches = _sampleDataSource.Groups.Where((group) => group.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1) return matches.First();
            return null;
        }

        public static async Task<Section3Item> GetItemAsync(string uniqueId)
        {
            await _sampleDataSource.GetAllSection3Async();
            // Simple linear search is acceptable for small data sets
            var matches = _sampleDataSource.Groups.SelectMany(group => group.Items).Where((item) => item.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1) return matches.First();
            return null;
        }

        private async Task GetAllSection3Async()
        {
            if (this._groups.Count != 0)
                return;

            Uri dataUri = new Uri("ms-appx:///Data/Section3Data.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["Groups"].GetArray();

            foreach (JsonValue groupValue in jsonArray)
            {
                JsonObject groupObject = groupValue.GetObject();
                Section3Model group = new Section3Model(groupObject["UniqueId"].GetString(),
                                                            groupObject["Title"].GetString(),
                                                            groupObject["Subtitle"].GetString(),
                                                            groupObject["ImagePath"].GetString(),
                                                            groupObject["Description"].GetString());

                foreach (JsonValue itemValue in groupObject["Items"].GetArray())
                {
                    JsonObject itemObject = itemValue.GetObject();
                    group.Items.Add(new Section3Item(itemObject["UniqueId"].GetString(),
                                                       itemObject["Title"].GetString(),
                                                       itemObject["Subtitle"].GetString(),
                                                       itemObject["ImagePath"].GetString(),
                                                       itemObject["Description"].GetString(),
                                                       itemObject["Content"].GetString()));
                }
                this.Groups.Add(group);
            }
        }
    }
}