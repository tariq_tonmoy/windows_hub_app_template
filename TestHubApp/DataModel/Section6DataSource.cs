﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
//using TestHubApp.Data;


namespace TestHubApp.DataModel
{
    public class Section6DataSource
    {
        private ObservableCollection<Section6Model> _section6Models = new ObservableCollection<Section6Model>();
        private static Section6DataSource _section6DataSource = new Section6DataSource();

        public ObservableCollection<Section6Model> Section6Models
        {
            get { return _section6Models; }
            set { _section6Models = value; }
        }

        public static async Task<Section6Model> GetSection6Async(string uniqueId)
        {
            await _section6DataSource.GetAllSection6Model();
            var match = _section6DataSource._section6Models.Where(x => x.UniqueId.Equals(uniqueId));
            if (match.Count() == 1)
            {
                return match.First();
            }
            return null;
        }

        private async Task GetAllSection6Model()
        {
              if (this._section6Models.Count != 0)
                return;

            Uri dataUri = new Uri("ms-appx:///Data/Section6Data.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["Section6"].GetArray();

            foreach (JsonValue section6Value in jsonArray)
            {
                JsonObject section6Object = section6Value.GetObject();
                Section6Model tempModel = new Section6Model()
                {
                    UniqueId = section6Object["UniqueId"].GetString(),
                    Header = section6Object["Header"].GetString(),
                    Body = this.GetSection6Body(section6Object)

                };

               

                this._section6Models.Add(tempModel);
            }
        }

        private ObservableCollection<Section6Body> GetSection6Body(JsonObject section6Object)
        {   
            if(section6Object==null)
                return null;
            ObservableCollection<Section6Body> _section6Bodies = new ObservableCollection<Section6Body>();
            foreach (JsonValue item in section6Object["Body"].GetArray())
            {
                JsonObject bodyObject = item.GetObject();
                Section6Body section6Body = new Section6Body() 
                {
                    Title=bodyObject["Title"].GetString(),
                    Description=bodyObject["Description"].GetString()

                };

                _section6Bodies.Add(section6Body);
                
            }

            return _section6Bodies;

        }
        
        

    }

    public class Section6Model
    {
        private string _uniqueId, _header;
        private ObservableCollection<Section6Body> _body = new ObservableCollection<Section6Body>();
        private double _gridWidth;

        public double GridWidth
        {
            get { return _gridWidth; }
            set { _gridWidth = value; }
        }
        public ObservableCollection<Section6Body> Body
        {
            get { return _body; }
            set { _body = value; }
        }
        public string UniqueId
        {
            get { return _uniqueId; }
            set { _uniqueId = value; }
        }

        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }


    }

    public class Section6Body
    {
        private string _title, _description;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }
}
