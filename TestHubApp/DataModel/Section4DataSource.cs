﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
//using TestHubApp.Data;
using Windows.Media;
using Windows.UI;
using System.Windows;
using Windows.UI.Xaml.Media;

namespace TestHubApp.DataModel
{
    public class Section4DataSource
    {
        private ObservableCollection<Section4Model> _section4Models = new ObservableCollection<Section4Model>();
        private static Section4DataSource _section4DataSource = new Section4DataSource();
        
        public ObservableCollection<Section4Model> Section4Models
        {
            get { return _section4Models; }
            set { _section4Models = value; }
        }



        public static async Task<Section4Image> GetSection4ImageAsync(string uniqueId)
        {
            await _section4DataSource.GetAllSection4Model();
            var match = _section4DataSource.Section4Models.SelectMany(x => x.Images).Where(x => x.UniqueId == uniqueId);
            if (match.Count() == 1) return match.First();
            return null;
        }
        public static async Task<Section4Model> GetSection4Async(string uniqueId)
        {
            await _section4DataSource.GetAllSection4Model();
            var match = _section4DataSource._section4Models.Where(x => x.UniqueId.Equals(uniqueId));
            if (match.Count() == 1)
            {
                return match.First();
            }
            return null;
        }

        private async Task GetAllSection4Model()
        {
            if (this._section4Models.Count != 0)
                return;

            Uri dataUri = new Uri("ms-appx:///Data/Section4Data.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["Section4"].GetArray();

            foreach (JsonValue section4Value in jsonArray)
            {
                JsonObject section4Object = section4Value.GetObject();
                Section4Model tempModel = new Section4Model()
                {
                    UniqueId = section4Object["UniqueId"].GetString(),
                    Header = section4Object["Header"].GetString(),
                    Images = this.GetSection4Image(section4Object)

                };



                this._section4Models.Add(tempModel);
            }
        }

        private ObservableCollection<Section4Image> GetSection4Image(JsonObject section4Object)
        {
            if (section4Object == null)
                return null;
            ObservableCollection<Section4Image> _section4Images = new ObservableCollection<Section4Image>();
            foreach (JsonValue item in section4Object["Image"].GetArray())
            {
                JsonObject imageObject = item.GetObject();
                Section4Image section4Image = new Section4Image()
                {
                    UniqueId=imageObject["UniqueId"].GetString(),
                    Title=imageObject["Title"].GetString(),
                    ImagePath = imageObject["ImagePath"].GetString(),
                    Description = imageObject["Description"].GetString()

                };

                _section4Images.Add(section4Image);

            }

            return _section4Images;

        }



    }

    public class Section4Model:CommonNotifyPropertyChanged
    {
        private string _uniqueId, _header, _selectedImage, _selectedDescription, _selectedTitle, _selectedImageId;
        SolidColorBrush _borderBackgroundBrush = new SolidColorBrush(Colors.Transparent);

        public SolidColorBrush BorderBackgroundBrush
        {
            get { return _borderBackgroundBrush; }
            set { SetProperty(ref _borderBackgroundBrush, value); }
        }
        public string SelectedImageId
        {
            get { return _selectedImageId; }
            set { _selectedImageId = value; }
        }
       
        public string SelectedTitle
        {
            get { return _selectedTitle; }
            set { SetProperty(ref _selectedTitle, value); }
        }

        public string SelectedDescription
        {
            get { return _selectedDescription; }
            set { SetProperty(ref _selectedDescription, value); }
        }

        public string SelectedImage
        {
            get { return _selectedImage; }
            set { SetProperty(ref _selectedImage, value); }
        }
        private ObservableCollection<Section4Image> _images = new ObservableCollection<Section4Image>();

        public ObservableCollection<Section4Image> Images
        {
            get { return _images; }
            set { _images = value; }
        }
        
        public string UniqueId
        {
            get { return _uniqueId; }
            set { _uniqueId = value; }
        }

        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }


    }

    public class Section4Image
    {
        private string _imagePath, _description, _uniqueId, _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string UniqueId
        {
            get { return _uniqueId; }
            set { _uniqueId = value; }
        }

        public string ImagePath
        {
            get { return _imagePath; }
            set { _imagePath = value; }
        }

     
        
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }
}
