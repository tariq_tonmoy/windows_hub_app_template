﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
//using TestHubApp.Data;

namespace TestHubApp.DataModel
{
    public class Section2DataSource
    {
        private static Section2DataSource _section2DataSource = new Section2DataSource();
        private ObservableCollection<Section2Model> _section2Models = new ObservableCollection<Section2Model>();

        public ObservableCollection<Section2Model> Section2Models
        {
            get { return _section2Models; }
            set { _section2Models = value; }
        }

        public static async Task<Section2Model> GetSection2Async(string uniqueID)
        {

            await _section2DataSource.GetAllSection2Async();
            if (_section2DataSource._section2Models.Where(x => x.UniqueId.Equals(uniqueID)).Count() == 1)
            {
                return _section2DataSource._section2Models.Where(x => x.UniqueId.Equals(uniqueID)).First();

            }
            else return null;
        }

        public async Task GetAllSection2Async()
        {
            if (_section2Models.Count != 0) return;
            Uri dataUri = new Uri("ms-appx:///Data/Section2Data.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["Section2"].GetArray();

            foreach (JsonValue section2Value in jsonArray)
            {
                JsonObject section2Object = section2Value.GetObject();
                Section2Model tempModel = new Section2Model()
                {
                    UniqueId = section2Object["UniqueId"].GetString(),
                    Header = section2Object["Header"].GetString(),
                    Title = section2Object["Title"].GetString(),
                    SubTitle = section2Object["SubTitle"].GetString(),
                    Description = section2Object["Description"].GetString(),
                    
                };
                this._section2Models.Add(tempModel);
            }
        }
    }
    public class Section2Model
    {
        private string _header, _uniqueId, _title, _subTitle, _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string SubTitle
        {
            get { return _subTitle; }
            set { _subTitle = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string UniqueId
        {
            get { return _uniqueId; }
            set { _uniqueId = value; }
        }

        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }


    }
}
