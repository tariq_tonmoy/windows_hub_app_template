﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
//using TestHubApp.Data;

namespace TestHubApp.DataModel
{
    public class Section1DataSource
    {
        private static Section1DataSource _section1DataSource = new Section1DataSource();
        private ObservableCollection<Section1Model> _section1Models = new ObservableCollection<Section1Model>();

        public ObservableCollection<Section1Model> Section1Models
        {
            get { return _section1Models; }
            set { _section1Models = value; }
        }

        public static async Task<Section1Model> GetSection1Async(string uniqueID)
        {

            await _section1DataSource.GetAllSection1Async();
            if(_section1DataSource._section1Models.Where(x=>x.UniqueId.Equals(uniqueID)).Count()==1)
            {
                return _section1DataSource._section1Models.Where(x=>x.UniqueId.Equals(uniqueID)).First();

            }
            else return null;
        }

        public async Task GetAllSection1Async()
        {
            if (_section1Models.Count != 0) return;
            Uri dataUri = new Uri("ms-appx:///Data/Section1Data.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["Section1"].GetArray();

            foreach (JsonValue section1Value in jsonArray)
            {
                JsonObject section1Object = section1Value.GetObject();
                Section1Model tempModel = new Section1Model()
                {
                    UniqueId = section1Object["UniqueId"].GetString(),
                    Header = section1Object["Header"].GetString(),
                    SubTitle = section1Object["SubTitle"].GetString(),
                    DescriptionHeader = section1Object["DescriptionHeader"].GetString(),
                    ImagePath = section1Object["ImagePath"].GetString(),
                    Description = section1Object["Description"].GetString()
                };
                this._section1Models.Add(tempModel);
            }
        }
                
                


    }

    public class Section1Model
    {
        private string _uniqueId, _header, _subTitle, _descriptionHeader, _description, _imagePath;

        public string ImagePath
        {
            get { return _imagePath; }
            set { _imagePath = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string DescriptionHeader
        {
            get { return _descriptionHeader; }
            set { _descriptionHeader = value; }
        }

        public string SubTitle
        {
            get { return _subTitle; }
            set { _subTitle = value; }
        }

        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }

        public string UniqueId
        {
            get { return _uniqueId; }
            set { _uniqueId = value; }
        }
    }
}
