﻿using TestHubApp.Common;
//using TestHubApp.Data;
using TestHubApp.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI;
using Windows.Media;



// The Hub Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=321224

namespace TestHubApp
{
    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class HubPage : Page
    {
       
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        static public Section1Model section1Model;
        static public Section2Model section2Model;
        static public Section3Model section3Model;
        static public Section4Model section4Model;
        static public Section5Model section5Model;


        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        public HubPage()
        {
            this.InitializeComponent();
            pageTitle.DataContext = App.myViewModel;
            App.myViewModel.TextForegroundBrush= new SolidColorBrush(Colors.Gray);

            
            


            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;

            
            

        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
       {
            // TODO: Create an appropriate data model for your problem domain to replace the sample data
            //var sampleDataGroup = await SampleDataSource.GetGroupAsync("Group-1");
            //this.DefaultViewModel["tempKey"] = sampleDataGroup;
            //this.Section3.DataContext = this.DefaultViewModel["tempKey"];

            section1Model = await Section1DataSource.GetSection1Async("ID1");
            this.Section1.DataContext = section1Model;

            section2Model = await Section2DataSource.GetSection2Async("ID2");
            this.Section2.DataContext = section2Model;

            section3Model = await Section3DataSource.GetSection3Async("Group-3");
            this.Section3.DataContext = section3Model;

            
            
            section4Model = await Section4DataSource.GetSection4Async("ID1");
            this.Section4.DataContext = section4Model;
            section4Model.SelectedImage = section4Model.Images[0].ImagePath;
            section4Model.SelectedDescription = section4Model.Images[0].Description;
            section4Model.SelectedTitle = section4Model.Images[0].Title;
            section4Model.SelectedImageId = section4Model.Images[0].UniqueId;

            section5Model = await Section5DataSource.GetSection5Async("ID1");
            this.Section5.DataContext = section5Model;

            //Section1.Visibility = Section2.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Invoked when a HubSection header is clicked.
        /// </summary>
        /// <param name="sender">The Hub that contains the HubSection whose header was clicked.</param>
        /// <param name="e">Event data that describes how the click was initiated.</param>
        void Hub_SectionHeaderClick(object sender, HubSectionHeaderClickEventArgs e)
        {
            HubSection section = e.Section;
            var group = section.DataContext;
            this.Frame.Navigate(typeof(SectionPage), ((Section3Model)group).UniqueId);
        }

        /// <summary>
        /// Invoked when an item within a section is clicked.
        /// </summary>
        /// <param name="sender">The GridView or ListView
        /// displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>
       
            

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

      
        private void mainHub_SectionsInViewChanged(object sender, SectionsInViewChangedEventArgs e)
        {
            string name = ((Hub)sender).SectionsInView[0].Name;
            if (name == "Section0") App.myViewModel.TextForegroundBrush = new SolidColorBrush(Colors.Gray);
            else App.myViewModel.TextForegroundBrush = new SolidColorBrush(Colors.White);
           


        }

        #region UserControl_EventHandlers

        #region Section4UserControl
        private void Section4Control_Loaded(object sender, RoutedEventArgs e)
        {
            Section4Control iStack = (Section4Control)sender;
            iStack.ItemClicked += iStack_ItemClicked;
            iStack.ImageTapped += iStack_ImageTapped;
            iStack.ImagePointerEntered += iStack_ImagePointerEntered;
            iStack.ImagePointerExited += iStack_ImagePointerExited;
        }

        void iStack_ImagePointerExited(object sender, PointerRoutedEventArgs e)
        {
            section4Model.BorderBackgroundBrush = new SolidColorBrush(Colors.Transparent);
        }

        void iStack_ImagePointerEntered(object sender, PointerRoutedEventArgs e)
        {
            section4Model.BorderBackgroundBrush = new SolidColorBrush(Colors.Gray);
        }

        void iStack_ImageTapped(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ImageMedia), section4Model);
        }

        async void iStack_ItemClicked(object sender, ItemClickEventArgs e)
        {
            string imageId = ((Section4Image)e.ClickedItem).UniqueId;

            Section4Image image = await Section4DataSource.GetSection4ImageAsync(imageId);
            section4Model.SelectedImage = image.ImagePath;
            section4Model.SelectedDescription = image.Description;
            section4Model.SelectedTitle = image.Title;
            section4Model.SelectedImageId = image.UniqueId;
        }


        #endregion Section4UserControl

        #region Section3UserControl
        private void Section3Control_Loaded(object sender, RoutedEventArgs e)
        {
            Section3Control s3Control = (Section3Control)sender;
            s3Control.itemClicked += s3Control_itemClicked;
        }

        void s3Control_itemClicked(object sender, ItemClickEventArgs e)
        {
            var itemId = ((Section3Item)e.ClickedItem).UniqueId;
            this.Frame.Navigate(typeof(ItemDetails), itemId);
        }

        #endregion SectionUserControl

        #region Section5UserControl
        private void Section5Control_Loaded(object sender, RoutedEventArgs e)
        {
            Section5Control s5Control = (Section5Control)sender;
            s5Control.VideoClicked += s5Control_VideoClicked;
        }

        void s5Control_VideoClicked(object sender, ItemClickEventArgs e)
        {
            this.Frame.Navigate(typeof(VideoMedia), ((Section5Video)e.ClickedItem).UniqueId);
        }
        #endregion Section5UserControl

        #endregion UserControl_EventHandlers

    }

    
}
