﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace TestHubApp
{
    public sealed partial class Section4Control : UserControl
    {
        public delegate void ActionClick(object sender, ItemClickEventArgs e);
        public delegate void ActionTapped(object sender, TappedRoutedEventArgs e);
        public delegate void ActionPointerEntered(object sender, PointerRoutedEventArgs e);
        public delegate void ActionPointerExited(object sender, PointerRoutedEventArgs e);

        public event ActionClick ItemClicked;
        public event ActionTapped ImageTapped;
        public event ActionPointerEntered ImagePointerEntered;
        public event ActionPointerExited ImagePointerExited;

        public Section4Control()
        {
            this.InitializeComponent();
        }

        private void ImageView_ItemClick(object sender, ItemClickEventArgs e)
        {

            if (ItemClicked != null)
            {
                ItemClicked(sender,e);
            }
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (ImageTapped != null)
            {
                ImageTapped(sender, e);
            }
        }

       
        private void imageBorder_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            if (ImagePointerEntered != null)
            {
                ImagePointerEntered(sender,e);
            }
        }

        private void imageBorder_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            if (ImagePointerExited != null)
            {
                ImagePointerExited(sender, e);
            }
        }

    }
}
