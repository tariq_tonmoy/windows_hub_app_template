﻿using TestHubApp.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI;
using System.Windows;
using System.Text;
using Windows.UI.Xaml.Markup;
using TestHubApp.DataModel;

namespace TestHubApp.Context
{
    public class GenerateParagraph
    {
        private double _totalChar = 0;

        public double TotalChar
        {
            get { return _totalChar; }
            set { _totalChar = value; }
        }
        public DataTemplate GetDataTemplate(Section6Model section6Model)
        {
            int totalChar = 0;
            foreach (Section6Body item in section6Model.Body)
            {
                totalChar += item.Title.Length;
                totalChar += item.Description.Length;
            }

            if (totalChar == 0) return null;

            totalChar = (int)Math.Ceiling((double)totalChar / 600.0);



            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\">");
            stringBuilder.Append("<Grid Width=\"{Binding GridWidth,Mode=OneWay}\" Background=\"#CC25292C\" >");
            stringBuilder.Append("<Grid.ColumnDefinitions>");
            for (int i = 0; i < totalChar; i++)
                stringBuilder.Append("<ColumnDefinition Width=\"400\"/>");
            stringBuilder.Append("</Grid.ColumnDefinitions>");


            string[] overflowContainer = new string[totalChar - 1];
            for (int i = 0; i < totalChar - 1; i++)
                overflowContainer[i] = "overflowContainer_" + i.ToString();



            if (totalChar > 1)
                stringBuilder.Append("<RichTextBlock Grid.Column=\"0\" IsTextSelectionEnabled=\"True\" TextAlignment=\"Justify\" OverflowContentTarget=\"{Binding ElementName=" + overflowContainer[0] + "}\" TextIndent=\"12\" FontSize=\"20\" FontFamily=\"Segoe UI\" Foreground=\"White\" Margin=\"24,24\" >");
            else
                stringBuilder.Append("<RichTextBlock Grid.Column=\"0\" IsTextSelectionEnabled=\"True\" TextAlignment=\"Justify\"  TextIndent=\"12\" FontSize=\"20\" FontFamily=\"Segoe UI\" Foreground=\"White\" Margin=\"24,24\">");

            foreach (Section6Body item in section6Model.Body)
            {
                stringBuilder.Append("<Paragraph>");
                stringBuilder.Append("<Bold>");
                stringBuilder.Append("<Span Foreground=\"DarkSlateBlue\" FontSize=\"30\" >");
                stringBuilder.Append(item.Title);
                stringBuilder.Append("</Span>");
                stringBuilder.Append("</Bold>");
                stringBuilder.Append("<LineBreak/>");
                stringBuilder.Append(item.Description);
                stringBuilder.Append("</Paragraph>");
            }
            stringBuilder.Append("</RichTextBlock>");

            for (int i = 0; i < totalChar - 2; i++)
            {
                stringBuilder.Append("<RichTextBlockOverflow  Name=\"" + overflowContainer[i] + "\" Grid.Column=\"" + (i + 1).ToString() + "\" Margin=\"24,24\" OverflowContentTarget=\"{Binding ElementName=" + overflowContainer[i + 1] + "}\"/>");
            }

            if (totalChar > 1)
                stringBuilder.Append("<RichTextBlockOverflow Name=\"" + overflowContainer[totalChar - 2] + "\" Grid.Column=\"" + (totalChar - 2 + 1).ToString() + "\" Margin=\"24,24\" />");


            stringBuilder.Append("</Grid>");

            stringBuilder.Append("</DataTemplate>");

            DataTemplate dataTemplate = (DataTemplate)XamlReader.Load(stringBuilder.ToString());
            this.TotalChar = totalChar;
            return dataTemplate;
        }
    }
}
