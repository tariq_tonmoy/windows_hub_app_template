﻿using TestHubApp.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TestHubApp.DataModel;
using System.Threading.Tasks;
using MyToolkit.Multimedia;
using Windows.UI;
using Windows.UI.Core;



// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace TestHubApp
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class VideoMedia : Page
    {
        private static DispatcherTimer timer = new DispatcherTimer();
        private static bool flag,tapFlag=false,btnFlag=false;
        private static WebView webView = new WebView();
        private static SliderViewModel _sliderViewModel = new SliderViewModel();
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private static MyViewModel mvm;

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public VideoMedia()
        {
        
            
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;

            StoryBoardFadeIn.Begin();
            tapFlag = true;

            CoreWindow.GetForCurrentThread().VisibilityChanged += VideoMedia_VisibilityChanged;
        }

        void VideoMedia_VisibilityChanged(CoreWindow sender, VisibilityChangedEventArgs args)
        {
            if (args.Visible)
            {
                timer.Start();
            }
            else timer.Stop();

        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            //webView = new WebView();
            ////webView.Height=150;
            ////webView.Width = 300;
            //DefaultViewModel["VideoSource"] = await Section5DataSource.GetSection5VideoAsync((string)e.NavigationParameter);
            
            //webView.Source = new Uri(((Section5Video)DefaultViewModel["VideoSource"]).VideoPath, UriKind.Absolute);
            //webView.Margin = new Thickness(12.0);
            
            //this.LayoutRoot.Children.Add(webView);


            mvm = new MyViewModel();
            mvm.MediaAction = "Assets/reverse.png";
            this.rewindButton.DataContext = mvm;

            mvm = new MyViewModel();
            mvm.MediaAction = "Assets/forward.png";
            this.forwardButton.DataContext = mvm;


            mvm = new MyViewModel();
            mvm.MediaAction = "Assets/pause.png";
            this.playButton.DataContext = mvm;
            flag = false;

            _sliderViewModel = new SliderViewModel();
            DefaultViewModel["VideoSource"] = await Section5DataSource.GetSection5VideoAsync((string)e.NavigationParameter);
            scrollGrid.DataContext = _sliderViewModel;

           // MediaElement mediaElement = new MediaElement();
            //Grid.SetRowSpan(mediaElement, 3);
            //LayoutRoot.Children.Add(mediaElement);

            Task<YouTubeUri> uri = YouTube.GetVideoUriAsync(((Section5Video)DefaultViewModel["VideoSource"]).YouTubeId, YouTubeQuality.QualityHigh);

            try
            {
                var url = await uri;
                if (url != null)
                {

                    progressRing.IsActive = false;
                    if (timer.Interval != null)
                        timer.Interval = new TimeSpan(0, 0, 1);
                    timer.Tick += timer_Tick;
                    timer.Start();
                    mediaElement.Source = url.Uri;

                    _sliderViewModel.CurrentValue = 0.0;








                    mediaElement.Play();
                    //System.Diagnostics.Debug.WriteLine(mediaElement.NaturalDuration.TimeSpan.TotalSeconds);


                }
                else
                {


                }
            }
            catch (Exception)
            {
                this.Frame.Navigate(typeof(HubPage));
            }
            



        }

       
        void timer_Tick(object sender, object e)
        {
            

            if (  mediaElement.CurrentState == MediaElementState.Playing)
            {
                
                _sliderViewModel.MaximumValue = mediaElement.NaturalDuration.TimeSpan.TotalSeconds;
                _sliderViewModel.CurrentValue = mediaElement.Position.TotalSeconds;
                _sliderViewModel.MaxTime = ((int)(mediaElement.NaturalDuration.TimeSpan.TotalSeconds / 60.0)).ToString() + ":" + (((int)mediaElement.NaturalDuration.TimeSpan.TotalSeconds) % 60).ToString();
                _sliderViewModel.MinTime = ((int)(mediaElement.Position.TotalSeconds / 60.0)).ToString() + ":" + (((int)mediaElement.Position.TotalSeconds) % 60).ToString();
                //nextValue+=1.0;
            }
            //if (nextValue > 0.0 && mediaElement.CurrentState == MediaElementState.Playing)
            //{

            //    _sliderViewModel.CurrentValue = mediaElement.Position.TotalSeconds + 1.0;
            //    _sliderViewModel.MinTime = ((int)(_sliderViewModel.CurrentValue / 60.0)).ToString() + ":" + (((int)_sliderViewModel.CurrentValue) % 60).ToString();

            //}
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _sliderViewModel = new SliderViewModel();
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            //_sliderViewModel = null;
            mediaElement.Stop();
            timer.Stop();
            //nextValue = -1.0;
            timer.Tick -= timer_Tick;
            //timer = null;
            
        }

        #endregion

      

        private void Play_Button_Click(object sender, RoutedEventArgs e)
        {
            btnFlag = true;
            if (mediaElement.CurrentState == MediaElementState.Playing || mediaElement.CurrentState == MediaElementState.Paused)
            {
                if (!flag)
                {
                    mvm.MediaAction = "Assets/play.png";
                    mediaElement.Pause();
                }
                else
                {
                    mvm.MediaAction = "Assets/pause.png";
                    mediaElement.Play();
                }
                flag = !flag;
            }
        }

        private void Rewind_Button_Click(object sender, RoutedEventArgs e)
        {
            btnFlag = true;
            if (mediaElement.CurrentState == MediaElementState.Playing || mediaElement.CurrentState == MediaElementState.Paused)
            {
                if (mediaElement.Position >= new TimeSpan(0, 0, 5))
                {
                    mediaElement.Position = mediaElement.Position - new TimeSpan(0, 0, 5);
                    _sliderViewModel.CurrentValue = mediaElement.Position.TotalSeconds;
                    _sliderViewModel.MinTime = _sliderViewModel.MinTime = ((int)((mediaElement.Position.TotalSeconds) / 60.0)).ToString() + ":" + (((int)(mediaElement.Position.TotalSeconds)) % 60).ToString();
                }
                else
                {
                    mediaElement.Position = new TimeSpan(0, 0, 0);
                    _sliderViewModel.CurrentValue = 0.0;
                    _sliderViewModel.MinTime = "0.0";
                }
            }
        }

        private void Forward_Button_Click(object sender, RoutedEventArgs e)
        {
            btnFlag = true;
            if (mediaElement.CurrentState == MediaElementState.Playing || mediaElement.CurrentState == MediaElementState.Paused)
            {
                if (mediaElement.Position <= mediaElement.NaturalDuration.TimeSpan)
                {
                    
                    mediaElement.Position = mediaElement.Position + new TimeSpan(0, 0, 5);



                    _sliderViewModel.CurrentValue = mediaElement.Position.TotalSeconds ;
                    _sliderViewModel.MinTime = _sliderViewModel.MinTime = ((int)((mediaElement.Position.TotalSeconds) / 60.0)).ToString() + ":" + (((int)(mediaElement.Position.TotalSeconds)) % 60).ToString();
                }
                else
                {
                    mediaElement.Position = mediaElement.NaturalDuration.TimeSpan;
                    _sliderViewModel.CurrentValue = mediaElement.NaturalDuration.TimeSpan.TotalSeconds;
                    _sliderViewModel.MinTime = ((int)(mediaElement.Position.TotalSeconds / 60.0)).ToString() + ":" + (((int)mediaElement.Position.TotalSeconds) % 60).ToString();
                }
            }
        }

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
           
            if (!btnFlag)
            {
                tapFlag = !tapFlag;

                if (tapFlag == false)
                {
                    StoryBoardFadeIn.Stop();
                    StoryBoardFadeOut.Begin();
                }
                else
                {
                    StoryBoardFadeOut.Stop();
                    StoryBoardFadeIn.Begin();
                }
            }
            else
            {
                btnFlag = false;
            }
        }

       
        
    }

   
}
